#include <math.h>

#include "mqtt_helper.h"

#define JSON_AND() ","
#define JSON_OBJECT(content) "{" content "}"
#define JSON_ARRAY(key, content) "\"" key "\":[" content "]"
#define JSON_STRING(key, value) "\"" key "\":\"" value "\""

#define CPP_JSON_OBJECT(content) arduino::String("{") content + "}";
#define CPP_JSON_ARRAY(key, content) + "\"" key "\":[" + content + "]"
#define CPP_JSON_STRING(key, value) + "\"" key "\":\"" + value + "\""

const char *MQTT_UPTIME_CONFIG_TOPIC = "homeassistant/sensor/fanduino/uptime/config";
#define MQTT_UPTIME_TOPIC "fanduino/uptime"
const char *MQTT_TEMP_CONFIG_TOPIC_PREFIX = "homeassistant/sensor/fanduino/temp_";
const char *MQTT_CONFIG_SUFFIX = "/config";
const char *MQTT_TEMP_TOPIC_PREFIX = "fanduino/temp_";
const char *MQTT_FAN_CONFIG_TOPIC_PREFIX = "homeassistant/sensor/fanduino/fan_";
const char *MQTT_FAN_TOPIC_PREFIX = "fanduino/fan_";
const char *MQTT_FAN_TOPIC_RPM_SUFFIX = "_rpm";
const char *MQTT_FAN_TOPIC_DUTY_CYCLE_SUFFIX = "_percent";
const char *MQTT_FAN_SOURCE_NAME_PREFIX = "fanduino/fan_";
const char *MQTT_SELECT_TOPIC_PREFIX = "homeassistant/select/";
const char *MQTT_FAN_SOURCE_SUFFIX = "_src";
#define MQTT_DEVICE() "\"device\":" JSON_OBJECT(JSON_ARRAY("ids", "\"fanduino_0\"") JSON_AND() JSON_STRING("mf","Hjemmelavet") JSON_AND() JSON_STRING("name","fanduino"))

const inline arduino::String sensor_addresses(DS18B20Sensor sensors[], size_t num_sensors)
{
    arduino::String s;

    for (size_t i = 0; i < num_sensors; i++)
    {
        s += "\"";
        s += sensors[i].address;
        s += "\",";
    }

    s.remove(s.length()-1);
    return s;
}

const arduino::String ds18b20_state_topic(const char* address)
{
    return arduino::String(MQTT_TEMP_TOPIC_PREFIX) + address;
}

const arduino::String fan_state_topic(size_t index)
{
    return arduino::String(MQTT_FAN_TOPIC_PREFIX) + index + MQTT_FAN_TOPIC_RPM_SUFFIX;
}

const arduino::String fan_duty_cycle_state_topic(size_t index)
{
    return arduino::String(MQTT_FAN_TOPIC_PREFIX) + index + MQTT_FAN_TOPIC_DUTY_CYCLE_SUFFIX;
}

const arduino::String fan_source_name_prefix(size_t index)
{
    return arduino::String(MQTT_FAN_SOURCE_NAME_PREFIX) + index + MQTT_FAN_SOURCE_SUFFIX;
}

const arduino::String fan_source_topic_prefix(size_t index)
{
    return arduino::String(MQTT_SELECT_TOPIC_PREFIX) + fan_source_name_prefix(index);
}

void MqttHelper::register_uptime()
{
    arduino::String message = JSON_OBJECT(
        JSON_STRING("name", "uptime") JSON_AND()
        JSON_STRING("uniq_id", "fanduino_uptime") JSON_AND()
        JSON_STRING("stat_t", MQTT_UPTIME_TOPIC) JSON_AND()
        JSON_STRING("unit_of_meas", "min") JSON_AND()
        JSON_STRING("dev_cla", "duration") JSON_AND()
        MQTT_DEVICE()
    );
    mqtt.beginMessage(MQTT_UPTIME_CONFIG_TOPIC, message.length(), true);
    mqtt.print(message);
	mqtt.endMessage();
}

void MqttHelper::register_ds18b20(const DS18B20Sensor& sensor)
{
    register_ds18b20(sensor.address);
}

void MqttHelper::register_ds18b20(const char* address)
{
    arduino::String topic(MQTT_TEMP_CONFIG_TOPIC_PREFIX);
    topic += address;
    topic += MQTT_CONFIG_SUFFIX;

    arduino::String message = CPP_JSON_OBJECT(
        CPP_JSON_STRING("name", "temp_"+address) JSON_AND()
        CPP_JSON_STRING("uniq_id", "fanduino_temp_"+address) JSON_AND()
        CPP_JSON_STRING("stat_t", ds18b20_state_topic(address)) JSON_AND()
        JSON_STRING("dev_cla", "temperature") JSON_AND()
        JSON_STRING("stat_cla", "measurement") JSON_AND()
        JSON_STRING("unit_of_meas", "°C") JSON_AND()
        MQTT_DEVICE()
    );
    mqtt.beginMessage(topic, message.length(), true);
    mqtt.print(message);
    mqtt.endMessage();
}

void MqttHelper::register_pwm_fan(size_t fan_index)
{
    {
        arduino::String topic(MQTT_FAN_CONFIG_TOPIC_PREFIX);
        topic += fan_index;
        topic += MQTT_FAN_TOPIC_RPM_SUFFIX;
        topic += MQTT_CONFIG_SUFFIX;

        arduino::String message = CPP_JSON_OBJECT(
            CPP_JSON_STRING("name", fan_state_topic(fan_index)) JSON_AND()
            CPP_JSON_STRING("uniq_id", "fanduino_fan_rpm_"+arduino::String(fan_index)) JSON_AND()
            CPP_JSON_STRING("stat_t", fan_state_topic(fan_index)) JSON_AND()
            JSON_STRING("unit_of_meas", "rpm") JSON_AND()
            JSON_STRING("stat_cla", "measurement") JSON_AND()
            MQTT_DEVICE()
        );
        // ("{\"name\":\"");
        // message += fan_state_topic(fan_index);
        // message += "\",\"stat_t\":\"";
        // message += fan_state_topic(fan_index);
        // message += "\"}";
        mqtt.beginMessage(topic, message.length(), true);
        mqtt.print(message);
        mqtt.endMessage();
    }
    {
        arduino::String topic(MQTT_FAN_CONFIG_TOPIC_PREFIX);
        topic += fan_index;
        topic += MQTT_FAN_TOPIC_DUTY_CYCLE_SUFFIX;
        topic += MQTT_CONFIG_SUFFIX;

        arduino::String message = CPP_JSON_OBJECT(
            CPP_JSON_STRING("name", fan_duty_cycle_state_topic(fan_index)) JSON_AND()
            CPP_JSON_STRING("uniq_id", "fanduino_fan_duty_"+arduino::String(fan_index)) JSON_AND()
            CPP_JSON_STRING("stat_t", fan_duty_cycle_state_topic(fan_index)) JSON_AND()
            JSON_STRING("unit_of_meas", "%") JSON_AND()
            JSON_STRING("stat_cla", "measurement") JSON_AND()
            MQTT_DEVICE()
        );
        // ("{\"name\":\"");
        // message += fan_duty_cycle_state_topic(fan_index);
        // message += "\",\"stat_t\":\"";
        // message += fan_duty_cycle_state_topic(fan_index);
        // message += "\"}";
        mqtt.beginMessage(topic, message.length(), true);
        mqtt.print(message);
        mqtt.endMessage();
    }
}

void MqttHelper::register_pwm_fan_sources(size_t fan_index, DS18B20Sensor sensors[], size_t num_sensors)
{
    arduino::String topic(fan_source_topic_prefix(fan_index));
    // topic += fan_index;
    topic += MQTT_CONFIG_SUFFIX;

    arduino::String message = CPP_JSON_OBJECT(
        CPP_JSON_STRING("name", fan_source_name_prefix(fan_index)) JSON_AND()
        CPP_JSON_STRING("uniq_id", "fanduino_fan_src_"+arduino::String(fan_index)) JSON_AND()
        CPP_JSON_STRING("~", fan_source_topic_prefix(fan_index)) JSON_AND()
        JSON_STRING("stat_t", "~/state") JSON_AND()
        JSON_STRING("cmd_t", "~/set") JSON_AND()
        CPP_JSON_ARRAY("options", sensor_addresses(sensors, num_sensors)) JSON_AND()
        MQTT_DEVICE()
    );
    // ("{\"name\":\"");
    // message += fan_source_name_prefix(fan_index);
    // message += "\",\"~\":\"";
    // message += fan_source_topic_prefix(fan_index);
    // message += "\",\"stat_t\":\"~/state\",\"cmd_t\":\"~/set\",\"options\":[";
    // for (size_t i = 0; i < num_sensors; i++)
    // {
    //     if (i>0) message += ",";
    //     message += "\"";
    //     message += sensors[i].address;
    //     message += "\"";
    // }
    // message += "]}";
    mqtt.beginMessage(topic, message.length(), true);
    mqtt.print(message);
    mqtt.endMessage();
}

void MqttHelper::subscribe_pwm_fan_source(size_t fan_index)
{
    mqtt.subscribe(fan_source_topic_prefix(fan_index)+"/set");
}

void MqttHelper::on_message_callback(int message_size)
{
    arduino::String topic = mqtt.messageTopic();
	Serial.print("Received MQTT message. [" + topic + "] length ");
	Serial.println(message_size);

	uint8_t buffer[message_size+1] = {};

	mqtt.read(buffer, message_size);
	Serial.println((char*)buffer);

    MessageEvent event;

    // A select was called
    if (topic.startsWith(MQTT_SELECT_TOPIC_PREFIX))
    {
        size_t offset = strlen(MQTT_SELECT_TOPIC_PREFIX);
        if (topic.startsWith(MQTT_FAN_SOURCE_NAME_PREFIX, offset) && topic.endsWith(MQTT_FAN_SOURCE_SUFFIX+arduino::String("/set")))
        {
            event.type = MessageType::PWM_SOURCE_ADDRESS;
            event.content = (char*)buffer;
            event.index = atoi(topic.substring(strlen(MQTT_FAN_SOURCE_NAME_PREFIX)+offset, topic.length()-strlen(MQTT_FAN_SOURCE_SUFFIX)-4).c_str());
        }
    }

    if (callback != NULL)
    {
        callback(event);
    }
}

void MqttHelper::on_event_callback(void (*callback)(MessageEvent))
{
    this->callback = callback;
}

void MqttHelper::update_uptime(uint16_t uptime)
{
    mqtt.beginMessage(MQTT_UPTIME_TOPIC);
	mqtt.print(uptime);
	mqtt.endMessage();
}

void MqttHelper::update_ds18b20(const DS18B20Sensor& sensor)
{
    update_ds18b20(sensor.address, sensor.temperature);
}

void MqttHelper::update_ds18b20(const char* address, float temperature)
{
    mqtt.beginMessage(ds18b20_state_topic(address));
    mqtt.print(round(temperature*10)/10);
    mqtt.endMessage();
}

void MqttHelper::update_pwm_fan(size_t fan_index, const PwmFan& pwm_fan)
{
    mqtt.beginMessage(fan_state_topic(fan_index));
    mqtt.print(pwm_fan.get_rpm_mean());
    mqtt.endMessage();

    mqtt.beginMessage(fan_duty_cycle_state_topic(fan_index));
    mqtt.print((pwm_fan.get_speed()*100) / 255);
    mqtt.endMessage();
}

void MqttHelper::update_pwm_fan_source(size_t fan_index, const DS18B20ShortAddress& source)
{
    mqtt.beginMessage(fan_source_topic_prefix(fan_index)+"/state");
    mqtt.print(source);
    mqtt.endMessage();
}
