#ifndef MQTT_HELPER_H_INCLUDED
#define MQTT_HELPER_H_INCLUDED

#include <ArduinoMqttClient.h>
#include "ds18b20_helper.h"
#include "pwm_fan.h"

enum class MessageType
{
    PWM_SOURCE_ADDRESS,
};

struct MessageEvent
{
    MessageType type;
    size_t index;
    arduino::String content;
};

class MqttHelper
{
    public:
        MqttHelper(MqttClient& client) : mqtt(client), callback(NULL) {}

        void register_uptime();
        void register_ds18b20(const DS18B20Sensor& sensor);
        void register_ds18b20(const char* address);
        void register_pwm_fan(size_t fan_index);
        void register_pwm_fan_sources(size_t fan_index, DS18B20Sensor sensors[], size_t num_sensors);

        void subscribe_pwm_fan_source(size_t fan_index);

        void on_message_callback(int message_size);
        void on_event_callback(void(*callback)(MessageEvent));

        void update_uptime(uint16_t uptime);
        void update_ds18b20(const DS18B20Sensor& sensor);
        void update_ds18b20(const char* address, float temperature);
        void update_pwm_fan(size_t fan_index, const PwmFan& pwm_fan);
        void update_pwm_fan_source(size_t fan_index, const DS18B20ShortAddress& source);
    private:
        MqttClient& mqtt;
        void(*callback)(MessageEvent);
};


#endif // MQTT_HELPER_H_INCLUDED