#include "fan_controller.h"

FanCurve FanController::sFanCurve
{
    // 0 / 255
    FanCurveEntry(0.0, 0), // <=0C => 0 duty cycle
    // 64 / 255
    FanCurveEntry(27.0, 64), // <=27C => 64 duty cycle
    // 96 / 255
    FanCurveEntry(30.0, 96), // <=30C => 96 duty cycle
    // 128 / 255
    FanCurveEntry(34.0, 128), // <=34C => 128 duty cycle
    // 196 / 255
    FanCurveEntry(37.0, 196), // <=37C => 196 duty cycle
    // 255 / 255
    FanCurveEntry(40.0, 255), // <=40C => 255 duty cycle
};

FanController::FanController()
    : mInitialised(false)
    , mFan(nullptr)
    , mTemperatureSensorAddress("00000000")
{}

void FanController::configure_fan(PwmFan& fan)
{
    mFan = &fan;
    mInitialised = true;
}

void FanController::configure_probe(const DS18B20ShortAddress& sensor_address)
{
    strcpy(mTemperatureSensorAddress, sensor_address);
    Serial.print("Binding fan to sensor ");
    Serial.println(sensor_address);
}

void FanController::adjust_speed(DS18B20Sensor sensors[], size_t num_sensors)
{
    if (!mInitialised)
    {
        return;
    }

    uint8_t oldSpeed = mFan->get_speed();
    uint8_t newSpeed = oldSpeed;
    DS18B20Sensor* sensor = nullptr;
    for (size_t i = 0; i < num_sensors; i++)
    {
        Serial.print("Comparing sensor ");
        Serial.print(sensors[i].address);
        Serial.print(" to ");
        Serial.println(mTemperatureSensorAddress);

        if (strcmp(sensors[i].address, mTemperatureSensorAddress) == 0)
        {
            sensor = &sensors[i];
            break;
        }
    }

    if (sensor != nullptr)
    {

        Serial.print("Temperature is ");
        Serial.println(sensor->temperature);
        for (size_t i = 1; i < FAN_CURVE_ENTRIES; i++)
        {
            // Serial.print(sensor->temperature);
            // Serial.print(">=");
            // Serial.print(sFanCurve[i].temperature);
            if (sensor->temperature < sFanCurve[i].temperature-0.5 && oldSpeed >= sFanCurve[i].demand)
            {
                Serial.print("Below threshold. Ramping down to ");
                newSpeed = sFanCurve[i-1].demand;
                Serial.println(newSpeed);
                break;
            }
            else if (sensor->temperature >= sFanCurve[i].temperature+0.5 && oldSpeed < sFanCurve[i].demand)
            {
                Serial.print("Above threshold. Ramping up to ");
                newSpeed = sFanCurve[i].demand;
                Serial.println(newSpeed);
            }
        }
    }

    Serial.print("Setting duty cycle to ");
    Serial.println(newSpeed);
    mFan->set_speed(newSpeed);
}

const DS18B20ShortAddress& FanController::get_probe_address()
{
    return mTemperatureSensorAddress;
}
