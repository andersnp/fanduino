#ifndef FAN_CONTROLLER_H_INCLUDED
#define FAN_CONTROLLER_H_INCLUDED

#include "ds18b20_helper.h"
#include "pwm_fan.h"

#define FAN_CURVE_ENTRIES 6

struct FanCurveEntry
{
    float temperature;
    uint8_t demand;

    FanCurveEntry(float temperature, uint8_t demand) : temperature(temperature), demand(demand) {}
};

typedef FanCurveEntry FanCurve[FAN_CURVE_ENTRIES];

class FanController
{
    public:
        FanController();

        void configure_fan(PwmFan& fan);
        void configure_probe(const DS18B20ShortAddress& sensor_address);

        void adjust_speed(DS18B20Sensor sensors[], size_t num_sensors);
        static void set_fan_curve(FanCurve fan_curve);

        const DS18B20ShortAddress& get_probe_address();
    private:
        static FanCurve sFanCurve;

        bool mInitialised;
        PwmFan* mFan;
        DS18B20ShortAddress mTemperatureSensorAddress;
};

struct FanData
{
    uint16_t magic;
    DS18B20ShortAddress sensor_address;
};

#endif // FAN_CONTROLLER_H_INCLUDED