#ifndef DS18B20_HELPER_H_INCLUDED
#define DS18B20_HELPER_H_INCLUDED

#include "Arduino.h"

const uint8_t ADDRESS_TRIM_LENGTH = 8;

typedef char DS18B20ShortAddress[ADDRESS_TRIM_LENGTH+1];

union DS18B20Address
{
    uint8_t bytes[8];
    uint64_t u64;
};

struct DS18B20Sensor
{
    DS18B20ShortAddress address { "00000000" };
    float temperature {0};
    float last_reported_temperature {0};
};

inline void convert_address(const uint8_t iBytes[8], DS18B20ShortAddress oAddress)
{
    uint8_t offset = ADDRESS_TRIM_LENGTH/2;
    for (auto j = offset; j < 8; j++)
    {
        char msb = iBytes[j] >> 4;
        char lsb = iBytes[j] % 16;
        oAddress[(j-offset)*2] = msb < 10 ? msb + '0' : msb + 'a' - 10;
        oAddress[((j-offset)*2)+1] = lsb < 10 ? lsb + '0' : lsb + 'a' - 10;
    }
    // Ensure this is terminated with a null character
    oAddress[ADDRESS_TRIM_LENGTH] = 0;
}

#endif // DS18B20_HELPER_H_INCLUDED