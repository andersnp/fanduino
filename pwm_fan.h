#ifndef PWM_FAN_H_INCLUDED
#define PWM_FAN_H_INCLUDED

#include "Arduino.h"

class PwmFan
{
    public:
        PwmFan(pin_size_t sense_pin, pin_size_t pwm_pin);

        void setup();
        
        uint8_t get_speed() const;
        void set_speed(uint8_t speed);

        void add_rpm_measurement();
        uint16_t get_rpm_mean() const;
        void reset_rpm_measurement();

    private:
        pin_size_t mSensePin;
        pin_size_t mPwmPin;
        uint8_t mSpeed;

        uint16_t mMeasurementsTotal;
        uint8_t mNumMeasurements;

        uint16_t measure_rpm() const;
};

#endif // PWM_FAN_H_INCLUDED