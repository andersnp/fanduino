#ifndef FAN_HELPER_H_INCLUDED
#define FAN_HELPER_H_INCLUDED

#include "Arduino.h"

uint16_t rpm(pin_size_t pin);

#endif // FAN_HELPER_H_INCLUDED