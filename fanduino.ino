#include <ArduinoMqttClient.h>
#include <WiFiNINA.h>
#include <utility/wifi_drv.h>
#include <DS18B20.h>
#include <EEPROM.h>

#include "mqtt_helper.h"
#include "secrets.h"
#include "ds18b20_helper.h"
#include "pwm_fan.h"
#include "fan_controller.h"

#define MAX_SENSORS 2
#define MAX_FAN 2
#define DS_SENSOR_PIN 2
#define ONE_MINUTE_IN_MILLIS 60000

#define EEPROM_FAN_DATA_ADDRESS 32
const uint16_t EEPROM_FAN_MAGIC = 0xface;
const size_t EEPROM_FAN_DATA_SIZE = sizeof(FanData);

const int MQTT_PORT = 1883;

WiFiClient wifi;
MqttClient mqtt(wifi);
MqttHelper mqttHelper(mqtt);
DS18B20 ds(DS_SENSOR_PIN);
uint8_t numDs;
DS18B20Sensor dsSensors[MAX_SENSORS];
PwmFan fans[] { PwmFan(4, 3), PwmFan(7, 6) };
FanController fanControllers[MAX_FAN];

void connect_wifi(bool loop = true)
{
	WiFi.end();
	int status = WL_IDLE_STATUS;
	do
	{
		Serial.write("Connecting to ");
		Serial.write(WIFI_SSID);
		Serial.write("\n");

		status = WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
		delay(5000);
		Serial.print("Status: ");
		Serial.println(status);
		Serial.print("IP: ");
		Serial.println(WiFi.localIP());
	} while (status != WL_CONNECTED && loop);
	Serial.println("Connected");
}

void connect_mqtt(bool loop = true)
{
	Serial.println("Connecting to MQTT");
	mqtt.setUsernamePassword(MQTT_USER, MQTT_PASSWORD);
	int status = 0;
	do
	{
		status = mqtt.connect(MQTT_BROKER, MQTT_PORT);
		if (status == 0)
		{
			Serial.println("Failed to connect to MQTT broker");
			Serial.println(mqtt.connectError());

			delay(1000);
		}

	} while (0 == status && loop);

	Serial.println("Connected");
}

void setup()
{
	pinMode(LED_BUILTIN, OUTPUT);
	digitalWrite(LED_BUILTIN, HIGH);
	for (size_t i = 0; i < sizeof(fans)/sizeof(PwmFan); i++)
	{
		fans[i].set_speed(128);
	}

	Serial.begin(115200);
	while (!Serial)
	{
		;
	}

	for (size_t i = 0; i < sizeof(fans)/sizeof(PwmFan); i++)
	{
		Serial.print("Initialise fan ");
		Serial.println(i);
		fans[i].setup();
	}

	if (WiFi.status() == WL_NO_MODULE)
	{
		Serial.write("Unable to find wifi module\n");
		while (true)
			;
	}

	arduino::String fw = WiFi.firmwareVersion();
	if (fw < WIFI_FIRMWARE_LATEST_VERSION)
	{
		Serial.write("Wifi firmware\n");
		Serial.write("Current version: ");
		Serial.println(fw);
		Serial.println("Latest version " WIFI_FIRMWARE_LATEST_VERSION);
	}

	connect_wifi();
	WiFi.setDNS(DNS);

	connect_mqtt();
	mqtt.onMessage(on_mqtt_messsage);
	mqttHelper.on_event_callback(on_mqtt_event);

	Serial.println("Registering to HASS");
	mqttHelper.register_uptime();

	for(uint8_t i = 0; ds.selectNext(); i++)
	{
		uint8_t address[8];
		ds.getAddress((uint8_t*)address);
		convert_address(address, dsSensors[i].address);
		Serial.print("Found DS18B20 sensor with address: ");
		Serial.println(dsSensors[i].address);

		mqttHelper.register_ds18b20(dsSensors[i]);
		numDs = i + 1;
	}
	Serial.write("Number of devices: ");
	Serial.println(numDs);

	for (size_t i = 0; i < sizeof(fans)/sizeof(PwmFan); i++)
	{
		FanData eepromData;
		DS18B20ShortAddress* sensorAddress;
		EEPROM.get(EEPROM_FAN_DATA_ADDRESS+EEPROM_FAN_DATA_SIZE*i, eepromData);
		if (eepromData.magic == EEPROM_FAN_MAGIC)
		{
			sensorAddress = &eepromData.sensor_address;
		}
		else
		{
			sensorAddress = &dsSensors[0].address;
		}

		bool sensorStillExists = false;
		for (size_t j = 0; j < MAX_SENSORS; j++)
		{
			if (strcmp(dsSensors[j].address, *sensorAddress) == 0)
			{
				sensorStillExists = true;
				break;
			}
		}

		if (sensorStillExists == false)
		{
			Serial.print("Sensor ");
			Serial.print(*sensorAddress);
			Serial.println(" was not found. Picking default");
			sensorAddress = &dsSensors[0].address;
		}

		fanControllers[i].configure_fan(fans[i]);
		fanControllers[i].configure_probe(*sensorAddress);
	}

	for (size_t i = 0; i < sizeof(fans)/sizeof(PwmFan); i++)
	{
		mqttHelper.register_pwm_fan(i);
		mqttHelper.register_pwm_fan_sources(i, dsSensors, numDs);
		mqttHelper.subscribe_pwm_fan_source(i);
	}
	digitalWrite(LED_BUILTIN, LOW);
}

void loop()
{
	static uint16_t interations = 0;
	digitalWrite(LED_BUILTIN, HIGH);
	mqtt.poll();

	auto timeStart = micros();
	mqttHelper.update_uptime(interations);
	++interations;

	for (auto& fan : fans)
	{
		fan.reset_rpm_measurement();
	}

	ds.resetSearch();
	DS18B20Address sensors[MAX_SENSORS] = {};
	for (uint8_t i = 0; i < MAX_SENSORS; i++)
	{
		if (ds.selectNext() == 0) break;
		ds.getAddress(sensors[i].bytes);
		Serial.print("Found: ");
		DS18B20ShortAddress s;
		convert_address(sensors[i].bytes, s);
		Serial.println(s);
		Serial.println(sensors[i].u64);
	}

	double temperatures[MAX_SENSORS] = {};
	uint16_t measurements = 0;
	Serial.println("Meas | Sensor | Reading | Average | Diff");
	do
	{
		auto timeLoopStart = micros();
		for (uint8_t i = 0; i < MAX_SENSORS; i++)
		{
			if (sensors[i].bytes == 0) break;
			ds.select(sensors[i].bytes);

			float reading = ds.getTempC();
			temperatures[i] += reading;
			Serial.print(measurements+1);
			Serial.print("\t\t");
			Serial.print(i);
			Serial.print("\t\t");
			Serial.print(reading);
			Serial.print("\t\t");
			Serial.print(temperatures[i]/(measurements+1));
			Serial.print("\t\t");
			Serial.println(reading - temperatures[i]/(measurements+1));
		}

		for (uint8_t i = 0; i < sizeof(fans)/sizeof(PwmFan); i++)
		{
			fans[i].add_rpm_measurement();
		}

		++measurements;
	} while (micros()-timeStart <= 30000000);

	// If we have more than MAX_SENSORS sensors connected, reset so we begin from #1
	ds.resetSearch();

	for(uint8_t i = 0; ds.selectNext() && i < MAX_SENSORS; i++)
	{
		bool found = false;
		DS18B20ShortAddress shortAddress;
		{
			uint8_t address[8];
			ds.getAddress(address);
			convert_address(address, shortAddress);
		}
		for (uint8_t j = 0; j < MAX_SENSORS; j++)
		{
			if (strcmp(dsSensors[j].address, shortAddress) == 0)
			{
				found = true;
				dsSensors[j].temperature = temperatures[i]/measurements;
				switch (ds.getFamilyCode())
				{
				case MODEL_DS18S20:
					Serial.println("Model: DS18S20/DS1820");
					break;
				case MODEL_DS1822:
					Serial.println("Model: DS1822");
					break;
				case MODEL_DS18B20:
					Serial.println("Model: DS18B20");
					break;
				default:
					Serial.println("Unrecognized Device");
					break;
				}

				{
					auto diff = dsSensors[j].temperature - dsSensors[j].last_reported_temperature;
					if (diff < 0) diff = -diff;
					if (diff > 0.10f)
					{
						mqttHelper.update_ds18b20(dsSensors[j]);
						dsSensors[j].last_reported_temperature = dsSensors[j].temperature;
					}
					else
					{
						Serial.print(dsSensors[j].address);
						Serial.print(": skipping: Diff ");
						Serial.println(diff);
					}

				}
				break;
			}
		}

		if (found == false)
		{
			Serial.print("Discovered a new sensor ");
			Serial.println(shortAddress);
		}
	}

	for (size_t i = 0; i < sizeof(fans)/sizeof(PwmFan); i++)
	{
		fanControllers[i].adjust_speed(dsSensors, MAX_SENSORS);
	}

	if (WiFi.status() != WL_CONNECTED)
	{
		connect_wifi(false);
	}
	if (!mqtt.connected())
	{
		connect_mqtt(false);
	}

	for (size_t i = 0; i < sizeof(fans)/sizeof(PwmFan); i++)
	{
		mqttHelper.update_pwm_fan(i, fans[i]);
		mqttHelper.update_pwm_fan_source(i, fanControllers[i].get_probe_address());
	}

	digitalWrite(LED_BUILTIN, LOW);
	auto timeNow = micros();
	if ((timeNow - timeStart)/1000 < ONE_MINUTE_IN_MILLIS)
	{
		Serial.write("Sleeping for ");
		Serial.println(ONE_MINUTE_IN_MILLIS-(timeNow-timeStart)/1000);
		delay(ONE_MINUTE_IN_MILLIS-(timeNow-timeStart)/1000);
	}
}

void on_mqtt_messsage(int message_size) { mqttHelper.on_message_callback(message_size); }

void on_mqtt_event(MessageEvent event)
{
	switch (event.type)
	{
		case MessageType::PWM_SOURCE_ADDRESS:
		{
			Serial.println("Received PWM_SRC_ADDR event.");
			Serial.print("Content ");
			Serial.println(event.content);
			Serial.print("Index ");
			Serial.println(event.index);
			FanData eepromData;
			eepromData.magic = EEPROM_FAN_MAGIC;
    		memcpy(&eepromData.sensor_address, event.content.c_str(), sizeof(DS18B20ShortAddress)/sizeof(char));

			fanControllers[event.index].configure_probe(eepromData.sensor_address);
			EEPROM.put(EEPROM_FAN_DATA_ADDRESS+EEPROM_FAN_DATA_SIZE*event.index, eepromData);
		}
	}
}