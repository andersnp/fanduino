#include "pwm_fan.h"
#include "fan_helper.h"

PwmFan::PwmFan(pin_size_t sense_pin, pin_size_t pwm_pin)
 : mSensePin(sense_pin)
 , mPwmPin(pwm_pin)
 , mSpeed(128)
{}

void PwmFan::setup()
{
	pinMode(mPwmPin, OUTPUT);
	pinMode(mSensePin, INPUT_PULLUP);
    analogWrite(mPwmPin, mSpeed);
}

void PwmFan::set_speed(uint8_t speed)
{
    mSpeed = speed;
    analogWrite(mPwmPin, speed);
}

uint8_t PwmFan::get_speed() const
{
    return mSpeed;
}

void PwmFan::add_rpm_measurement()
{
    if (get_speed() > 0)
    {
        mMeasurementsTotal += measure_rpm();
    }
    ++mNumMeasurements;
}

uint16_t PwmFan::get_rpm_mean() const
{
    return mMeasurementsTotal / mNumMeasurements;
}

void PwmFan::reset_rpm_measurement()
{
    mMeasurementsTotal = 0;
    mNumMeasurements = 0;
}

uint16_t PwmFan::measure_rpm() const
{
    return rpm(mSensePin);
}