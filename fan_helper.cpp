#include "fan_helper.h"

#define TWO_HUNDRED_MILLIS_IN_MICROS 200000

uint16_t rpm(pin_size_t pin)
{
	auto maxTime = micros() + TWO_HUNDRED_MILLIS_IN_MICROS;
	// Wait for pin to go low
	while(digitalRead(pin) == HIGH && micros() < maxTime);

	auto start = micros();

	while(digitalRead(pin) == LOW && micros() < maxTime) ;
	while(digitalRead(pin) == HIGH && micros() < maxTime) ;
	while(digitalRead(pin) == LOW && micros() < maxTime) ;
	while(digitalRead(pin) == HIGH && micros() < maxTime) ;

	auto end = micros();

	if (end > maxTime)
	{
		return 0;
	}

	auto duration = end - start;

	// Serial.print("One revolution took ");
	// Serial.print(duration);
	// Serial.print("us\n");

	return 60000000 / duration;
}